using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;

public class Shooting : MonoBehaviourPunCallbacks
{
    public Camera camera;
    public GameObject hitEffectPrefab;

    private GameObject playerPrefabList;

    [Header("HP Related Stuff")]
    public float startHealth = 100;
    private float health;
    public Image healthBar;

    [Header("Score")]
    public int score = 0;

    private Animator animator;
    // Start is called before the first frame update
    void Start()
    {
        health = startHealth;
        healthBar.fillAmount = health / startHealth;
        animator = this.GetComponent<Animator>();
        playerPrefabList = GameObject.Find("PlayerPrefabList");
        this.gameObject.transform.parent = playerPrefabList.transform;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Fire()
    {
        RaycastHit hit;
        Ray ray = camera.ViewportPointToRay(new Vector3(0.5f, 0.5f));

        if (Physics.Raycast(ray, out hit, 200))
        {
            Debug.Log(hit.collider.gameObject.name);
            photonView.RPC("CreateHitEffects", RpcTarget.All, hit.point);

            if (hit.collider.gameObject.CompareTag("Player") && !hit.collider.gameObject.GetComponent<PhotonView>().IsMine)
            {
                hit.collider.gameObject.GetComponent<PhotonView>().RPC("TakeDamage", RpcTarget.AllBuffered, 25);
            }
        }
    }

    [PunRPC]
    public void TakeDamage(int damage, PhotonMessageInfo info)
    {
        this.health -= damage;
        this.healthBar.fillAmount = health / startHealth;


        if (health <= 0)
        {
            Die();
            GameObject playerUi = GameObject.Find("Player UI(Clone)");
            Debug.Log(info.Sender.NickName + " killed " + info.photonView.Owner.NickName);
            playerUi.GetComponent<KilledListUI>().AddNewPrefabToList(info.Sender.NickName, info.photonView.Owner.NickName);
            GainPointIfKiller(info.Sender.ActorNumber);
        }
    }

    [PunRPC]
    public void CreateHitEffects(Vector3 position)
    {
        GameObject hitEffectGameObject = Instantiate(hitEffectPrefab, position, Quaternion.identity);
        Destroy(hitEffectGameObject, 0.2f);
    }

    public void Die()
    {
        if (photonView.IsMine)
        {
            animator.SetBool("isDead", true);
            StartCoroutine(RespawnCountdown());
        }
    }

    IEnumerator RespawnCountdown()
    {
        GameObject respawnText = GameObject.Find("Respawn Text");
        float respawnTime = 5.0f;

        while (respawnTime > 0)
        {
            yield return new WaitForSeconds(1.0f);
            respawnTime--;

            transform.GetComponent<PlayerMovementController>().enabled = false;
            respawnText.GetComponent<Text>().text = "You are killed, Respawning in " + respawnTime.ToString(".00");
        }

        animator.SetBool("isDead", false);
        respawnText.GetComponent<Text>().text = "";

        this.transform.position = SpawnPointList.instance.GetRandSpawn().position;
        transform.GetComponent<PlayerMovementController>().enabled = true;

        photonView.RPC("RegainHealth", RpcTarget.AllBuffered);
    }


    [PunRPC]
    public void RegainHealth()
    {
        health = startHealth;
        healthBar.fillAmount = health / startHealth;
    }

    public void GainPointIfKiller(int actorNum)
    {
        foreach (Transform child in playerPrefabList.transform)
        {
            if (child.gameObject.GetComponent<PhotonView>().Owner.ActorNumber == actorNum)
            {
                child.gameObject.GetComponent<Shooting>().GainPoint();
            }
        }
    }

    public void GainPoint()
    {
        int PointsToWin = 10;
        this.score++;

        if (score >= PointsToWin)
        {
            DisplayWinner();
        }
    }

    public void DisplayWinner()
    {
        GameObject winnderText = GameObject.Find("Winner Text");
        winnderText.GetComponent<Text>().text = photonView.Owner.NickName + " has won the match! Returning to Lobby...";
        StartCoroutine(ReturnToLobby());
    }

    IEnumerator ReturnToLobby()
    {
        yield return new WaitForSeconds(3.0f);
        GameManager.instance.ReturnPlayerBackToLobbyScene();
    }
}
