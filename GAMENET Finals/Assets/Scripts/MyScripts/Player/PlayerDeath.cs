using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class PlayerDeath : MonoBehaviourPunCallbacks
{
    [SerializeField] PlayerData playerData;
    Transform respawnPoint;

    private int killOrder = 0;//Used for checking total players killed
    private float currentHealth;

    private bool isAlive = true;

    private Text announcementUiText;

    void Start()
    {
        respawnPoint = playerData.getRespawnPoint();
    }

    private void OnEnable()
    {
        PhotonNetwork.NetworkingClient.EventReceived += OnDeathEvent;
    }

    private void OnDisable()
    {
        PhotonNetwork.NetworkingClient.EventReceived -= OnDeathEvent;
    }

    void OnDeathEvent(EventData photonEvent)
    {
        if (photonEvent.Code == (byte)Constants.RaiseEventsCode.LOSDeathEvent || photonEvent.Code == (byte)Constants.RaiseEventsCode.DMDeathEvent)
        {
            object[] data = (object[])photonEvent.CustomData;

            string playerKilledName = (string)data[0];
            string KillerPlayerName = (string)data[1];
            killOrder = (int)data[2];//DO I STILL NEED THIS??????
            int killerId = (int)data[3];

            string whoKilledWhoText;

            if (killerId == photonView.ViewID)
                whoKilledWhoText = "YOU killed " + playerKilledName;
            else
                whoKilledWhoText = KillerPlayerName + " killed " + playerKilledName;

            if (photonView.IsMine)
                GameManager.instance.CreateNewWhoKilledWho(whoKilledWhoText);

            //GameManager.instance.CreateNewWhoKilledWho(whoKilledWhoText);

            //announcementUiText = RacingGameManager.instance.announcementText;
            //announcementUiText.enabled = true;

            //if (killOrder >= (PhotonNetwork.CurrentRoom.PlayerCount - 1)) // If all players have been killed except one
            //{
            //    GameManager.instance.announcementText.text = KillerPlayerName + " Wins!";
            //}

            //StartCoroutine(ClearAnnouncementText(3.0f)); //Clears text
        }
        //else if (photonEvent.Code == (byte)Constants.RaiseEventsCode.DMDeathEvent)
        //{ 
        
        //}
    }

    //[PunRPC]
    //public void TakeDamage(int damage, PhotonMessageInfo info)
    //{
    //    currentHealth -= damage;
    //    Debug.Log(currentHealth);

    //    healthbar.fillAmount = currentHealth / maxHealth;

    //    if (currentHealth <= 0 && isAlive == true)
    //    {
    //        Die(info.Sender.NickName, info.Sender.ActorNumber, info.photonView.Owner.NickName);
    //    }
    //}

    private void SendDeathEvent(string killerPlayerName, int killerPlayerId, string killedPlayerName, int killedPlayerId, bool isOutOfLives)
    {
        if (photonView.IsMine)
        {
            //Debug.Log("You Died");

            //isAlive = false;

            //// Disable input
            //GetComponent<VehicleMovement>().isControlEnabled = false;

            //if (GetComponent<ProjectileWeapon>())
            //{
            //    GetComponent<ProjectileWeapon>().enabled = false;
            //}

            //else if (GetComponent<RaycastWeapon>())
            //{
            //    GetComponent<RaycastWeapon>().enabled = false;
            //}

            if (PhotonNetwork.CurrentRoom.CustomProperties.ContainsValue(Constants.GameMode1))//LastOneStanding
            {
                //if (playerData.lives < 0)
                //    killOrder++;

                // Event data
                object[] data = new object[] { killedPlayerName, killerPlayerName, killOrder, killerPlayerId, killedPlayerId, isOutOfLives};

                RaiseEventOptions raiseEventOptions = new RaiseEventOptions
                {
                    Receivers = ReceiverGroup.All,
                    CachingOption = EventCaching.AddToRoomCache
                };

                SendOptions sendOption = new SendOptions
                {
                    Reliability = false
                };

                PhotonNetwork.RaiseEvent((byte)Constants.RaiseEventsCode.LOSDeathEvent, data, raiseEventOptions, sendOption);

                //StartCoroutine(Delay(3.0f));

                //gameObject.GetComponent<PlayerSetup>().camera.enabled = false;
                //RacingGameManager.instance.spectatorCamera.enabled = true;
            }
            else if (PhotonNetwork.CurrentRoom.CustomProperties.ContainsValue(Constants.GameMode2))//Deathmatch
            {
                // Event data
                object[] data = new object[] { killedPlayerName, killerPlayerName, killOrder, killerPlayerId, killedPlayerId, isOutOfLives};

                RaiseEventOptions raiseEventOptions = new RaiseEventOptions
                {
                    Receivers = ReceiverGroup.All,
                    CachingOption = EventCaching.AddToRoomCache
                };

                SendOptions sendOption = new SendOptions
                {
                    Reliability = false
                };

                PhotonNetwork.RaiseEvent((byte)Constants.RaiseEventsCode.DMDeathEvent, data, raiseEventOptions, sendOption);
            }
        }
    }

    IEnumerator Delay(float time)
    {
        yield return new WaitForSeconds(time);
    }

    IEnumerator ClearAnnouncementText(float time)
    {
        yield return new WaitForSeconds(time);

        announcementUiText.text = "";
        announcementUiText.enabled = false;
    }


    [PunRPC]
    public void Die(PhotonMessageInfo info)
    {
        if (isAlive)
        {
            if (PhotonNetwork.CurrentRoom.CustomProperties.ContainsValue(Constants.GameMode1))//LastOneStanding Limited Lives
            {
                if (playerData.lives > 1)//Player killed but has lives
                {

                    playerData.lives--;
                    SendDeathEvent(info.Sender.NickName, info.Sender.ActorNumber, info.photonView.Owner.NickName, info.photonView.Owner.ActorNumber, false);

                    if (photonView.IsMine)
                        Respawn();
                }
                else//Player killed and has no lives
                {
                    Debug.Log("Die");
                    killOrder++;

                    SendDeathEvent(info.Sender.NickName, info.Sender.ActorNumber, info.photonView.Owner.NickName, info.photonView.Owner.ActorNumber, true);
                    isAlive = false;
                    DeathDisableComps();
                }
            }
            else if (PhotonNetwork.CurrentRoom.CustomProperties.ContainsValue(Constants.GameMode2))//Deathmatch Unlimited Lives
            {
                Debug.Log("Die");
                SendDeathEvent(info.Sender.NickName, info.Sender.ActorNumber, info.photonView.Owner.NickName, info.photonView.Owner.ActorNumber, false);

                if (photonView.IsMine)
                    Respawn();
            }
        }
    }

    public void Respawn()
    {
        gameObject.transform.position = respawnPoint.position;
        isAlive = true;
    }

    private void DeathDisableComps()
    {
        if (GetComponent<PlayerComponentManager>())
        {
            GetComponent<PlayerComponentManager>().DeathDisableComponents();
        }
    }
}
