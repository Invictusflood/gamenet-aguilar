using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;

public class TakingDamage : MonoBehaviourPunCallbacks
{
    [SerializeField]
    Image healthBar;

    private float startHealth = 100;
    public float Health;
    // Start is called before the first frame update
    void Start()
    {
        Health = startHealth;
        healthBar.fillAmount = Health / startHealth;
    }

    [PunRPC]
    public void TakeDamage(int damage)
    {
        Health -= damage;
        Debug.Log(Health);

        healthBar.fillAmount = Health / startHealth;
        if (Health <= 0)
        {
            Die();
        }
    }

    private void Die()
    {
        if(photonView.IsMine)//to make sure only the owning player will leave, not cause others to do so
            GameManager.instance.LeaveRoom();
    }
}
