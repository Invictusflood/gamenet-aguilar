﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class RaycastWeapon : MonoBehaviourPunCallbacks
{
    public Camera camera;
    public GameObject bulletHitEffectPrefab;

    [SerializeField]
    public float fireRate = 0.1f;
    private float timer = 0;

    [SerializeField] private int damage;

    // Update is called once per frame
    void Update()
    {
        if (timer < fireRate)
        {
            timer += Time.deltaTime;
        }

        if (Input.GetButton("Fire1") && timer > fireRate)
        {
            timer = 0.0f;
            Ray ray = camera.ViewportPointToRay(new Vector3(0.5f, 0.5f));
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, 100))
            {
                Debug.Log(hit.collider.gameObject.name);

                // Same thing from m2
                photonView.RPC("CreateHitEffects", RpcTarget.All, hit.point);

                if (hit.collider.gameObject.CompareTag("Player") && !hit.collider.gameObject.GetComponent<PhotonView>().IsMine)
                {
                    hit.collider.gameObject.GetComponent<PhotonView>().RPC("TakeDamage", RpcTarget.AllBuffered, damage);
                }
            }
        }
    }

    [PunRPC]
    public void CreateHitEffects(Vector3 position)
    {
        GameObject hitEffectGameObject = Instantiate(bulletHitEffectPrefab, position, Quaternion.identity);
        Destroy(hitEffectGameObject, 0.2f);
    }
}
