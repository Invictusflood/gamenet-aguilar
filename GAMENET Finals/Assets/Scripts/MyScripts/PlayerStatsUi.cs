using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PlayerStatsUi : MonoBehaviour
{
    [SerializeField] GameObject lifeIconContainer;
    [SerializeField] GameObject killScoreTextGameObject;
    [SerializeField] GameObject playerNameTextGameObject;
    [SerializeField] GameObject heartIconPrefab;



    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetPlayerName(string playerName)
    {
        TextMeshProUGUI textMesh = playerNameTextGameObject.GetComponent<TextMeshProUGUI>();
        textMesh.text = playerName;
    }

    public void EnableHeartIcons()
    {
        lifeIconContainer.SetActive(true);
        killScoreTextGameObject.SetActive(false);
    }

    public void EnableKillScore()
    {
        lifeIconContainer.SetActive(false);
        killScoreTextGameObject.SetActive(true);
    }

    public void SetKillScoreText(int killScore)
    {
        TextMeshProUGUI textMesh = killScoreTextGameObject.GetComponent<TextMeshProUGUI>();
        textMesh.text = "Kills: " + killScore;
    }

    public void AddLifeIcon()
    {
        GameObject heartIcon = Instantiate(heartIconPrefab);
        heartIcon.transform.SetParent(lifeIconContainer.transform, false);
    }

    public void RemoveLifeIcon()
    {
        if (lifeIconContainer.transform.childCount > 0)
        {
            Destroy(lifeIconContainer.transform.GetChild(0).gameObject);
        }
        else 
        {
            Debug.Log("PlayerStatsUi: HeartIconContainer is Empty!");
        }
    }
}
