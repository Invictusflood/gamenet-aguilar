using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerArrowCountUI : MonoBehaviour
{
    [SerializeField] private GameObject ArrowUiPrefab;
    [SerializeField] private GameObject ArrowCountUi;
    public float showUiTime;

    // Start is called before the first frame update
    void Start()
    {
        ShowArrowUi(showUiTime);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void AddArrowCountUi()
    {
        //Vector3 uiSize = new Vector3(0.5f, 0.5f, 0.5f);
        GameObject newArrowUi = Instantiate(ArrowUiPrefab);
        //newArrowUi.transform.localScale = uiSize;
        newArrowUi.transform.SetParent(ArrowCountUi.transform, false);

        ShowArrowUi(showUiTime);
    }

    public void RemoveArrowCountUi()
    {
        if (ArrowCountUi.transform.childCount > 0)
        {
            Destroy(ArrowCountUi.transform.GetChild(0).gameObject);
        }

        ShowArrowUi(showUiTime);
    }

    private void ShowArrowUi(float seconds)
    {
        ArrowCountUi.SetActive(true);
        Invoke("HideArrowUi", seconds);
    }

    private void HideArrowUi()
    {
        ArrowCountUi.SetActive(false);
    }
}
