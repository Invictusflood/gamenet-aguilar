using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPointList : MonoBehaviour
{
    public static SpawnPointList instance { get; private set; }

    public List<Transform> spawnPoints;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            //DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public Transform GetRandSpawn()
    {
        if (spawnPoints.Count > 0)
        {
            int randNum = Random.Range(0, spawnPoints.Count);
            return spawnPoints[randNum];
        }
        else
        {
            Debug.LogError("SpawnPointList is empty");
            return null;
        }
    }
}
