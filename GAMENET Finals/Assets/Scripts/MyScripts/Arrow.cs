using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;


public class Arrow : MonoBehaviourPunCallbacks
{
    [SerializeField] private LayerMask platformLayerMask;

    Rigidbody2D rb;
    public bool hasHit;
    public bool canDamage = false;
    public float ignorePlayerTime;//So arrow wont hurt owner initially

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        Invoke("EnableDamaging", ignorePlayerTime);
    }

    // Update is called once per frame
    void Update()
    {
        if (!hasHit)
        {
            float angle = Mathf.Atan2(rb.velocity.y, rb.velocity.x) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Level"))
        {
            hasHit = true;
            rb.velocity = Vector2.zero;
            rb.isKinematic = true;
            Debug.Log("hit level");
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Level"))
        {
            hasHit = true;
            rb.velocity = Vector2.zero;
            rb.isKinematic = true;
            Debug.Log("hit level");
        }

        if (collision.gameObject.CompareTag("Player"))
        {
            if (hasHit)
            {
                Debug.Log("Ammo Gain trigger");

                if (collision.gameObject.GetPhotonView().IsMine)//Only add ammo if the player is yours
                {
                    //collision.gameObject.GetComponent<PlayerData>().ammo++;
                    collision.gameObject.GetPhotonView().RPC("AddAmmo", RpcTarget.AllBuffered);
                }

                Destroy(gameObject);
                //DestroySelf();
                //photonView.RPC("DestroySelf", RpcTarget.AllBuffered);
            }
            else
            {
                if (canDamage)
                {
                    Debug.Log("Damage trigger");
                    if(photonView.IsMine)//Fixes error of calling the rpc multipe times across all users
                        collision.gameObject.GetPhotonView().RPC("Die", RpcTarget.AllBuffered); //Die funtion is from the PlayerDeath script
                    //Damage
                }
            }

            Debug.Log("player trigger");
        }
    }

    private void EnableDamaging()
    {
        canDamage = true;
    }

    [PunRPC]
    public void DestroySelf()
    {
        //PhotonNetwork.Destroy(gameObject);
        Destroy(gameObject);
    }
}
