﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class Projectile : MonoBehaviourPunCallbacks
{
    [SerializeField] private int lifespan;
    [SerializeField] private int launchSpeed;
    [SerializeField] private int damage;

    private Rigidbody rigidbody;
    private GameObject ownerGameObject;
    private Vector3 movementForce;

    // Start is called before the first frame update
    void Start()
    {
        rigidbody = GetComponent<Rigidbody>();

        movementForce = launchSpeed * transform.forward;

        Destroy(gameObject, lifespan);
    }

    public void SetOwner(GameObject parent)
    {
        ownerGameObject = parent;
    }

    private void FixedUpdate()
    {
        rigidbody.AddForce(movementForce);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject != ownerGameObject)
        {
            Debug.Log(other.gameObject.name + " was hit by bullet");

            if (other.gameObject.CompareTag("Player") && !other.gameObject.GetComponent<PhotonView>().IsMine)
            {
                other.gameObject.GetComponent<PhotonView>().RPC("TakeDamage", RpcTarget.AllBuffered, damage);
            }

            Destroy(gameObject);
        }
    }
}
