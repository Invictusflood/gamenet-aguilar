﻿using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Health : MonoBehaviourPunCallbacks
{
    [SerializeField] private float maxHealth;
    [SerializeField] Image healthbar;
    private int killOrder = 0;//Used for checking total players killed
    private float currentHealth;

    private bool isAlive = true;

    private Text announcementUiText;

    public enum RaiseEventsCode
    {
        WhoKilledWho = 1
    }

    private void OnEnable()
    {
        PhotonNetwork.NetworkingClient.EventReceived += OnDeathEvent;

        InitHealth();
    }

    private void OnDisable()
    {
        PhotonNetwork.NetworkingClient.EventReceived -= OnDeathEvent;
    }

    void InitHealth()
    {
        currentHealth = maxHealth;
        healthbar.fillAmount = currentHealth / maxHealth;
    }

    void OnDeathEvent(EventData photonEvent)
    {
        if (photonEvent.Code == (byte)RaiseEventsCode.WhoKilledWho)
        {
            object[] data = (object[])photonEvent.CustomData;

            string playerKilledName = (string)data[0];
            string KillerPlayerName = (string)data[1];
            killOrder = (int)data[2];
            int killerId = (int)data[3];

            GameObject killUi = RacingGameManager.instance.killedTextUi[killOrder - 1];
            killUi.SetActive(true);

            announcementUiText = RacingGameManager.instance.announcementText;
            announcementUiText.enabled = true;

            if (killerId == photonView.ViewID)
            {
                // This is you
                killUi.GetComponent<Text>().text = "You ---> " + playerKilledName;
                killUi.GetComponent<Text>().color = Color.red;
            }
            else
            {
                killUi.GetComponent<Text>().text = KillerPlayerName + " ---> " + playerKilledName;
                announcementUiText.text = playerKilledName + " was eliminated";
            }

            if (killOrder >= (PhotonNetwork.CurrentRoom.PlayerCount - 1)) // If all players have been killed except one
            {
                announcementUiText.text = KillerPlayerName + " Wins!";
            }

            StartCoroutine(ClearAnnouncementText(3.0f)); //Clears text
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        InitHealth();
    }

    [PunRPC]
    public void TakeDamage(int damage, PhotonMessageInfo info)
    {
        currentHealth -= damage;
        Debug.Log(currentHealth);

        healthbar.fillAmount = currentHealth / maxHealth;

        if (currentHealth <= 0 && isAlive == true)
        {
            Die(info.Sender.NickName, info.Sender.ActorNumber, info.photonView.Owner.NickName);
        }
    }

    private void Die(string killerPlayerName, int killerId, string killedPlayerName)
    {
        if (photonView.IsMine)
        {
            Debug.Log("You Died");

            isAlive = false;

            // Disable input
            GetComponent<VehicleMovement>().isControlEnabled = false;

            if (GetComponent<ProjectileWeapon>())
            {
                GetComponent<ProjectileWeapon>().enabled = false;
            }

            else if (GetComponent<RaycastWeapon>())
            {
                GetComponent<RaycastWeapon>().enabled = false;
            }

            killOrder++;

            // Event data
            object[] data = new object[] { killedPlayerName, killerPlayerName, killOrder, killerId };

            RaiseEventOptions raiseEventOptions = new RaiseEventOptions
            {
                Receivers = ReceiverGroup.All,
                CachingOption = EventCaching.AddToRoomCache
            };

            SendOptions sendOption = new SendOptions
            {
                Reliability = false
            };

            PhotonNetwork.RaiseEvent((byte)RaiseEventsCode.WhoKilledWho, data, raiseEventOptions, sendOption);

            StartCoroutine(Delay(3.0f));

            gameObject.GetComponent<PlayerSetup>().camera.enabled = false;
            RacingGameManager.instance.spectatorCamera.enabled = true;
        }
    }

    IEnumerator Delay(float time)
    {
        yield return new WaitForSeconds(time);
    }

    IEnumerator ClearAnnouncementText(float time)
    {
        yield return new WaitForSeconds(time);

        announcementUiText.text = "";
        announcementUiText.enabled = false;
    }
}
