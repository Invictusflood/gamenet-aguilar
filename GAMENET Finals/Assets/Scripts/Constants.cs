﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Constants
{
    public const string PLAYER_READY = "isPlayerReady";
    public const string PLAYER_SELECTION_NUMBER = "playerSelectionNumber";

    public const string GameMode1 = "los"; //Last one standing
    public const string GameMode2 = "dm"; //deathmatch

    public enum RaiseEventsCode
    {
        LOSDeathEvent = 1,
        DMDeathEvent = 2,
        DMKillDataUpdatedEvent = 3

    }
}
