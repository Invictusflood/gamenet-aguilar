using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;

public class PlayerComponentManager : MonoBehaviourPunCallbacks
{
    //[SerializeField] private Text nameText;

    [SerializeField] Bow bowComponent;
    [SerializeField] SpriteRenderer playerGraphic;
    [SerializeField] SpriteRenderer bowGraphic;

    // Start is called before the first frame update
    void Start()
    {
        //this.camera = transform.Find("Camera").GetComponent<Camera>();

        if (GetComponent<PlayerMovement>())
        {
            GetComponent<PlayerMovement>().enabled = photonView.IsMine;
        }

        //if (GetComponent<Rigidbody2D>())
        //{
        //    GetComponent<Rigidbody2D>().isKinematic = !photonView.IsMine;
        //}

        if (bowComponent)
        {
            bowComponent.enabled = photonView.IsMine;
        }

        if (GetComponent<PlayerDeath>())
        {
            GetComponent<PlayerDeath>().enabled = photonView.IsMine;
        }

        //camera.enabled = photonView.IsMine;

        //nameText.text = photonView.Owner.NickName;

        //if (PhotonNetwork.CurrentRoom.CustomProperties.ContainsValue(Constants.GameMode1))
        //{
        //    bool setter = false;
        //    // Disable weapons for everyone
        //    if (GetComponent<ProjectileWeapon>())
        //    {
        //        GetComponent<ProjectileWeapon>().enabled = setter;
        //    }

        //    else if (GetComponent<RaycastWeapon>())
        //    {
        //        GetComponent<RaycastWeapon>().enabled = setter;
        //    }

        //    GetComponent<HealthComponent>().enabled = setter;
        //}
        //else if (PhotonNetwork.CurrentRoom.CustomProperties.ContainsValue(Constants.GameMode2))
        //{
        //    bool setter = photonView.IsMine;
        //    // Enable shooting if it is yours
        //    if (GetComponent<ProjectileWeapon>())
        //    {
        //        GetComponent<ProjectileWeapon>().enabled = setter;
        //    }

        //    else if (GetComponent<RaycastWeapon>())
        //    {
        //        GetComponent<RaycastWeapon>().enabled = setter;
        //    }

        //    GetComponent<HealthComponent>().enabled = setter;
        //}
    }

    public void DeathDisableComponents()
    {
        if (GetComponent<PlayerMovement>())
        {
            GetComponent<PlayerMovement>().enabled = false;
        }

        if (GetComponent<BoxCollider2D>())
        {
            GetComponent<BoxCollider2D>().enabled = false;
        }

        if (GetComponent<Rigidbody2D>())
        {
            GetComponent<Rigidbody2D>().isKinematic = true;
        }

        if (bowComponent)
        {
            bowComponent.enabled = false;
        }

        if (playerGraphic)
        {
            playerGraphic.enabled = false;
        }

        if (bowGraphic)
        {
            bowGraphic.enabled = false;
        }

        //if (GetComponent<PlayerDeath>())
        //{
        //    GetComponent<PlayerDeath>().enabled = false;
        //}
    }
}
