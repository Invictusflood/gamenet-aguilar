﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class ProjectileWeapon : MonoBehaviourPunCallbacks
{
    public GameObject firePoint;
    public GameObject projectilePrefab;

    [SerializeField]
    public float fireRate;
    private float timer = 0;

    private GameObject spawnedMissle;

    // Update is called once per frame
    void Update()
    {
        if (timer < fireRate)
        {
            timer += Time.deltaTime;
        }

        if (Input.GetButton("Fire1") && timer > fireRate)// Left Mouse Click
        {
            // instantiate projectile
            photonView.RPC("Fire", RpcTarget.All, firePoint.transform.position);
            if (spawnedMissle != null)
            {
                Debug.Log("gave " + gameObject.name + " reference to the missle");
                spawnedMissle.GetComponent<Projectile>().SetOwner(this.gameObject);
            }

            timer = 0;
        }
    }

    [PunRPC]
    public void Fire(Vector3 position)
    {
        if (photonView.IsMine)
        {
            spawnedMissle = PhotonNetwork.Instantiate(projectilePrefab.name, position, transform.rotation);
        }
    }
}
