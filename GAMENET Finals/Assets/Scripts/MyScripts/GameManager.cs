using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;
using TMPro;
using Photon.Realtime;
using ExitGames.Client.Photon;

public class GameManager : MonoBehaviourPunCallbacks
{
    [SerializeField] int killsToWinDM = 10;
    [SerializeField] bool gameHasBeenWon = false;

    public GameObject[] playerPrefabs;
    public Transform[] playerSpawns;
    public GameObject[] finisherTextUi;
    public GameObject[] killedTextUi;

    public GameObject[] playerStatsUiPrefabs;
    public List<GameObject> playerStatsUiGameObjects;
    public GameObject playerStatsUiContainer;

    public GameObject WhoKilledContainer;
    public GameObject WhoKilledUiPrefab;

    [SerializeField] private Dictionary<int, string> alivePlayers;

    //reference to the spectator camera
    public Camera spectatorCamera;

    public static GameManager instance = null;

    public TextMeshProUGUI announcementText;

    public List<GameObject> lapTriggers = new List<GameObject>();

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }

        //DontDestroyOnLoad(gameObject);

        alivePlayers = new Dictionary<int, string>();
    }

    private void OnEnable()
    {
        PhotonNetwork.NetworkingClient.EventReceived += OnDeathEvent;
    }

    private void OnDisable()
    {
        PhotonNetwork.NetworkingClient.EventReceived -= OnDeathEvent;
    }

    // Start is called before the first frame update
    void Start()
    {


        //GameManager.instance.CreateNewWhoKilledWho("test");
        if (PhotonNetwork.IsConnectedAndReady)
        {
            Debug.Log("Player connected and Ready!"); // test

            foreach (Player player in PhotonNetwork.PlayerList)
            {
                CreateNewPlayerStatsUi(player);
                PlayerToAlivePlayers(player);
            }

            //object playerSelectionNumber;
            //if (PhotonNetwork.LocalPlayer.CustomProperties.TryGetValue(Constants.PLAYER_SELECTION_NUMBER, out playerSelectionNumber))
            //{
            //    Debug.Log("Player selection #: " + (int)playerSelectionNumber);

            //    int actorNumber = PhotonNetwork.LocalPlayer.ActorNumber;
            //    Vector3 instantiatePosition = startingPositions[actorNumber - 1].position;
            //    PhotonNetwork.Instantiate(vehiclePrefabs[(int)playerSelectionNumber].name, instantiatePosition, Quaternion.identity);
            //}

            //Debug.Log("Player selection #: " + (int)playerSelectionNumber);
            int actorNumber = PhotonNetwork.LocalPlayer.ActorNumber;
            Vector3 instantiatePosition = playerSpawns[actorNumber - 1].position;

            GameObject spawnedPlayer = PhotonNetwork.Instantiate(playerPrefabs[actorNumber - 1].name, instantiatePosition, Quaternion.identity);
            spawnedPlayer.GetComponent<PlayerData>().setRespawnPoint(playerSpawns[actorNumber - 1]);

            PlayerData spawnedPlayerData = spawnedPlayer.GetComponent<PlayerData>();


            photonView.RPC("SetPlayerStatsUiName", RpcTarget.AllBuffered, actorNumber - 1, PhotonNetwork.LocalPlayer.NickName);
            //CHECK IF THIS WORKS!!!!!!
            if (PhotonNetwork.CurrentRoom.CustomProperties.ContainsValue(Constants.GameMode1))//LastOneStanding Limited Lives, show lives
            {
                photonView.RPC("SetUpPlayerStatsUiLifeIcons", RpcTarget.AllBuffered, actorNumber - 1, spawnedPlayerData.lives);
            }

        }

        //foreach (GameObject go in finisherTextUi)
        //{
        //    go.SetActive(false);
        //}

        //foreach (GameObject go in killedTextUi)
        //{
        //    go.SetActive(false);
        //}

        //spectatorCamera.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {

    }

    void CreateNewPlayerStatsUi(Player player)
    {
        GameObject newPlayerStatsUiGO = Instantiate(playerStatsUiPrefabs[player.ActorNumber - 1]);
        newPlayerStatsUiGO.transform.SetParent(playerStatsUiContainer.transform, false);
        playerStatsUiGameObjects.Add(newPlayerStatsUiGO);


        PlayerStatsUi playerStatsUi = newPlayerStatsUiGO.GetComponent<PlayerStatsUi>();
        if (PhotonNetwork.CurrentRoom.CustomProperties.ContainsValue(Constants.GameMode1))//LastOneStanding Limited Lives, show lives
        {
            playerStatsUi.EnableHeartIcons();
        }
        else if (PhotonNetwork.CurrentRoom.CustomProperties.ContainsValue(Constants.GameMode2))//Deathmatch Unlimited Lives, Track kills
        {
            playerStatsUi.EnableKillScore();
        }
    }

    [PunRPC]
    void SetUpPlayerStatsUiLifeIcons(int index, int playerLives)
    {
        PlayerStatsUi playerStatsUi = playerStatsUiGameObjects[index].GetComponent<PlayerStatsUi>();
        for (int i = 0; i < playerLives; i++)
        {
            playerStatsUi.AddLifeIcon();
        }
    }

    [PunRPC]
    void SetPlayerStatsUiName(int index, string playerName)
    {
        PlayerStatsUi playerStatsUi = playerStatsUiGameObjects[index].GetComponent<PlayerStatsUi>();
        playerStatsUi.SetPlayerName(playerName);
    }

    void RemoveLifeIcon(int index)
    {
        PlayerStatsUi playerStatsUi = playerStatsUiGameObjects[index].GetComponent<PlayerStatsUi>();
        playerStatsUi.RemoveLifeIcon();
    }

    void UpdateTotalKillsText(int index, int TotalKills)
    {
        PlayerStatsUi playerStatsUi = playerStatsUiGameObjects[index].GetComponent<PlayerStatsUi>();
        playerStatsUi.SetKillScoreText(TotalKills);
    }

    //private void AddPlayerToAlivePlayers(int playerActorNum, string playerName)
    //{ 

    //}

    void OnDeathEvent(EventData photonEvent)
    {
        Debug.Log("GameManager: Event Reveived");
        if (photonEvent.Code == (byte)Constants.RaiseEventsCode.LOSDeathEvent)
        {
            Debug.Log("GameManager: LOS Death Event Received");
            object[] data = (object[])photonEvent.CustomData;

            bool isPlayerOutOfLives = (bool)data[5];

            if (isPlayerOutOfLives)
            {
                //string playerKilledName = (string)data[0];
                int killedId = (int)data[4];

                RemoveLifeIcon(killedId - 1);
                alivePlayers.Remove(killedId);

                if (alivePlayers.Count == 1)
                {
                    DisplayLastOneStanding();
                    Invoke("ReturnToLobby", 2.0f);
                }
                else if (alivePlayers.Count == 0)
                {
                    announcementText.text = "Seems like the only player left... killed theirself?";
                    Invoke("ReturnToLobby", 2.0f);
                }
            }
            else
            {
                int killedId = (int)data[4];
                RemoveLifeIcon(killedId - 1);
            }
        }
        else if (photonEvent.Code == (byte)Constants.RaiseEventsCode.DMKillDataUpdatedEvent)
        {
            Debug.Log("GameManager: DMKillDataUpdatedEvent Received");
            object[] data = (object[])photonEvent.CustomData;
            int killerId = (int)data[0];
            string playerName = (string)data[1];
            int totalKills = (int)data[2];

            UpdateTotalKillsText(killerId - 1, totalKills);

            if (totalKills >= killsToWinDM && !gameHasBeenWon)
            {
                gameHasBeenWon = true;
                announcementText.text = playerName + " has 10 or more kills, and Wins!";
                Invoke("ReturnToLobby", 2.0f);
            }
        }
    }

    private void ReturnToLobby()
    {
        if (PhotonNetwork.IsMasterClient)
        {
            PhotonNetwork.LoadLevel("LobbyScene");
        }
    }

    private void DisplayLastOneStanding()
    {
        foreach (Player player in PhotonNetwork.PlayerList)
        {
            if (alivePlayers.ContainsKey(player.ActorNumber))
            {
                announcementText.text = alivePlayers[player.ActorNumber] + " Wins!";
                return;
            }
        }
    }

    public void CreateNewWhoKilledWho(string newText)
    {
        GameObject go = Instantiate(WhoKilledUiPrefab);
        go.GetComponent<WhoKilledUi>().SetText(newText);
        go.transform.SetParent(WhoKilledContainer.transform, false);
    }

    private void PlayerToAlivePlayers(Player player)
    {
        alivePlayers.Add(player.ActorNumber, player.NickName);
        Debug.Log(player.ActorNumber + ":" + player.NickName + " has been added to alivePlayer list");
    }

    #region Photon Callbacks

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        Debug.Log(newPlayer.ActorNumber + ":" + newPlayer.NickName + " has joined mid-game ");
        alivePlayers.Add(newPlayer.ActorNumber, newPlayer.NickName);
    }

    //Sadly does not fire on application quit
    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        Debug.Log(otherPlayer.ActorNumber + " : " + otherPlayer.NickName + " has left mid-game ");
        alivePlayers.Remove(otherPlayer.ActorNumber);

        if (alivePlayers.Count == 1)
        {
            DisplayLastOneStanding();
        }
    }

    //Sadly does not fire on application quit
    public void OnPlayerDisconnected(Player player)
    {
        Debug.Log(player.ActorNumber + " : " + player.NickName + " disconnected mid-game ");
        alivePlayers.Remove(player.ActorNumber);

        if (alivePlayers.Count == 1)
        {
            DisplayLastOneStanding();
        }
    }


    //[PunRPC]
    //private void OnPlayerQuitAppMidGame(Player leftPlayer)
    //{
    //    Debug.Log(leftPlayer.ActorNumber + " : " + leftPlayer.NickName + " has left mid-game ");
    //    alivePlayers.Remove(leftPlayer.ActorNumber);

    //    if (alivePlayers.Count == 1)
    //    {
    //        DisplayLastOneStanding();
    //    }
    //}

    private void OnApplicationQuit()
    {
        //photonView.RPC("OnPlayerQuitAppMidGame", RpcTarget.AllBuffered, photonView.Owner);
        PhotonNetwork.LeaveRoom();
    }
    #endregion
}
