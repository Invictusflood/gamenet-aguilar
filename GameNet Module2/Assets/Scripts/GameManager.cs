using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class GameManager : MonoBehaviour
{
    public static GameManager instance { get; private set; }

    public GameObject playerPrefab;
    public GameObject playerList;

    //public int clientPlayerScore;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            //DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        if (PhotonNetwork.IsConnectedAndReady)
        {
            int randomRangeX = Random.Range(-10, 10);
            int randomRangeZ = Random.Range(-10, 10);
            PhotonNetwork.Instantiate(playerPrefab.name, SpawnPointList.instance.GetRandSpawn().position, Quaternion.identity);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ReturnPlayerBackToLobbyScene()
    {
        PhotonNetwork.LoadLevel("LobbyScene");
    }
}
