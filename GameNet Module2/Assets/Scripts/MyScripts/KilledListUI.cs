using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KilledListUI : MonoBehaviour
{
    public List<GameObject> whoKilledPrefabList;
    public GameObject whoKilledContent;
    public GameObject whoKilledPrefab;

    public void AddNewPrefabToList(string attackerName, string killedName)
    {
        if (whoKilledPrefabList.Count >= 4)
        {
            Destroy(whoKilledPrefabList[0]);
            whoKilledPrefabList.RemoveAt(0);
        }


        GameObject newPrefab = Instantiate(whoKilledPrefab);
        newPrefab.transform.SetParent(whoKilledContent.transform, false);

        newPrefab.transform.Find("AttackerNameText").GetComponent<Text>().text = attackerName;
        newPrefab.transform.Find("KilledNameText").GetComponent<Text>().text = killedName;

        whoKilledPrefabList.Add(newPrefab);
    }
}
