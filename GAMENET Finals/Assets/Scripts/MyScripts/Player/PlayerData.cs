using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using ExitGames.Client.Photon;
using Photon.Realtime;

public class PlayerData : MonoBehaviourPunCallbacks
{
    [SerializeField] PlayerArrowCountUI playerUi;
    [SerializeField] Transform respawnPoint;
    // Start is called before the first frame update
    public int ammo;
    public int kills;
    public int lives;

    void Start()
    {
        if (playerUi)
        {
            for (int i = 0; i < ammo; i++)
            {
                playerUi.AddArrowCountUi();
            }
        }
    }

    private void OnEnable()
    {
        PhotonNetwork.NetworkingClient.EventReceived += OnDeathEvent;
    }

    private void OnDisable()
    {
        PhotonNetwork.NetworkingClient.EventReceived -= OnDeathEvent;
    }

    void OnDeathEvent(EventData photonEvent)
    {
        if (photonEvent.Code == (byte)Constants.RaiseEventsCode.DMDeathEvent)
        {
            object[] data = (object[])photonEvent.CustomData;

            int killerId = (int)data[3];
            int killedId = (int)data[4];

            if (photonView.Owner.ActorNumber == killerId)//If you are the killer
            {
                if (killerId != killedId)//If you didnt kill yourself
                {
                    kills++;
                    SendKillDataUpdatedEvent();
                }
            }
        }

    }

    void SendKillDataUpdatedEvent()
    {
        if (photonView.IsMine)
        {
            Player thisPlayer = photonView.Owner;
            object[] data = new object[] { thisPlayer.ActorNumber, thisPlayer.NickName, kills};

            RaiseEventOptions raiseEventOptions = new RaiseEventOptions
            {
                Receivers = ReceiverGroup.All,
                CachingOption = EventCaching.AddToRoomCache
            };

            SendOptions sendOption = new SendOptions
            {
                Reliability = false
            };

            PhotonNetwork.RaiseEvent((byte)Constants.RaiseEventsCode.DMKillDataUpdatedEvent, data, raiseEventOptions, sendOption);
        }
    }

    public void setRespawnPoint(Transform spawnPoint)
    {
        respawnPoint = spawnPoint;
    }

    public Transform getRespawnPoint()
    {
        return respawnPoint;
    }

    [PunRPC]
    public void AddAmmo()
    {
        ammo++;
        if (playerUi)
            playerUi.AddArrowCountUi();
    }

    [PunRPC]
    public void RemoveAmmo()
    { 
        ammo--;
        if (playerUi)
            playerUi.RemoveArrowCountUi();
    }
}
