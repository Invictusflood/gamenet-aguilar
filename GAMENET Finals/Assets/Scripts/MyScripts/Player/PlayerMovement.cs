using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField] private LayerMask platformLayerMask;
    [SerializeField] public SpriteRenderer playerSprite;
    [SerializeField] public GameObject playerBody;

    public Rigidbody2D rb2d;
    private BoxCollider2D boxCollider2D;
    public bool isFacingRight = true;

    public bool isGrounded;

    [Header("Wall Detection")]
    public bool isTouchingWall;
    public Transform wallCheck;
    public float wallRayCastDist;
    public bool wallSliding;
    public float wallSlidingSpeed;

    bool wallJumping;
    public float xWallForce;
    public float yWallForce;
    public float wallJumpTime;

    private Vector2 FacingDir = (Vector2)Vector3.right;


    [System.Serializable]
    public class PlayerMotionStats
    {
        public float Speed = 10f;
        public float JumpVelocity = 15f;
    }

    public PlayerMotionStats playerMovement = new PlayerMotionStats();

    // Start is called before the first frame update
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        rb2d.drag = 2;

        boxCollider2D = GetComponent<BoxCollider2D>();
    }


    // Update is called once per frame
    void Update()
    {
        SetIfGrounded();
        SetIfTouchingWall();
    }

    void LateUpdate()
    {
        movePlayer();

        Jumping();

        WallSliding();

        WallJumping();
    }

    private void movePlayer()
    {
        if (Input.GetAxis("Horizontal") > 0 && isFacingRight == false)
        {
            //playerSprite.flipX = false;
            Flip();
        }
        else if (Input.GetAxis("Horizontal") < 0 && isFacingRight == true)
        {
            //playerSprite.flipX = true;
            Flip();
        }

        Vector2 movementVector = new Vector2(Input.GetAxis("Horizontal") * playerMovement.Speed, rb2d.velocity.y);
        rb2d.velocity = movementVector;
    }

    private void Flip()
    {
        playerBody.transform.localScale = new Vector3(-playerBody.transform.localScale.x, playerBody.transform.localScale.y, playerBody.transform.localScale.z);
        isFacingRight = !isFacingRight;
    }

    private void Jumping()
    {
        if (Input.GetButtonDown("Jump") && isGrounded)
        {
            rb2d.velocity = Vector2.up * playerMovement.JumpVelocity;
        }
    }

    public void SetIfTouchingWall()
    {
        Vector3 RayCastDir;
        if (isFacingRight)
            RayCastDir = wallCheck.right;
        else
            RayCastDir = -wallCheck.right;

        RaycastHit2D wallInfo = Physics2D.Raycast(wallCheck.position, RayCastDir, wallRayCastDist, platformLayerMask);
        Debug.DrawRay(wallCheck.position, RayCastDir * wallRayCastDist, Color.green);

        isTouchingWall = wallInfo.collider != null;
    }

    private void WallSliding()
    {

        float input = Input.GetAxis("Horizontal");

        if (isTouchingWall && !isGrounded && input != 0)
        {
            wallSliding = true;
        }
        else 
        {
            wallSliding = false;
        }

        if (wallSliding)
        {
            rb2d.velocity = new Vector2(rb2d.velocity.x, Mathf.Clamp(rb2d.velocity.y, -wallSlidingSpeed, float.MaxValue));
        }
    }

    private void WallJumping()
    {
        float input = Input.GetAxis("Horizontal");

        if (Input.GetButtonDown("Jump") && wallSliding)
        {
            wallJumping = true;
            //rb2d.velocity += new Vector2(xWallForce * -input, yWallForce);
            Invoke("SetWallJumpingToFalse", wallJumpTime);
        }

        if (wallJumping)
        {
            rb2d.velocity = new Vector2(xWallForce * -input, yWallForce);
        }
    }

    public void SetIfGrounded()
    {
        RaycastHit2D raycastHit2d = Physics2D.BoxCast(boxCollider2D.bounds.center, boxCollider2D.bounds.size, 0f, Vector2.down, .5f, platformLayerMask);

        //if (raycastHit2d.collider == null)
        //    wallSliding = false;

        isGrounded = raycastHit2d.collider != null;
    }

    void SetWallJumpingToFalse()
    {
        wallJumping = false;
    }
}
