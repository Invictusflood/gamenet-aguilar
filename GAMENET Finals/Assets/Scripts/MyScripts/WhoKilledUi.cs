using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class WhoKilledUi : MonoBehaviour
{
    [SerializeField] float timeTillDestroy;
    [SerializeField] TextMeshProUGUI text;
    // Start is called before the first frame update
    void Start()
    {
        Invoke("DestroySelf", timeTillDestroy);
    }

    public void SetText(string newText)
    {
        text.text = newText;
    }

    private void DestroySelf()
    {
        Destroy(gameObject);
    }
}
