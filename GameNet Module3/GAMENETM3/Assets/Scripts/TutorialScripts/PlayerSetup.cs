﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;
public class PlayerSetup : MonoBehaviourPunCallbacks
{
    public Camera camera;

    [SerializeField] private Text nameText;
    
    // Start is called before the first frame update
    void Start()
    {
        this.camera = transform.Find("Camera").GetComponent<Camera>();

        if (GetComponent<VehicleMovement>())
        {
            GetComponent<VehicleMovement>().enabled = photonView.IsMine;
        }

        if(GetComponent<LapController>())
        {
            GetComponent<LapController>().enabled = photonView.IsMine;
        }

        camera.enabled = photonView.IsMine;

        nameText.text = photonView.Owner.NickName;

        if (PhotonNetwork.CurrentRoom.CustomProperties.ContainsValue("rc"))
        {
            bool setter = false;
            // Disable weapons for everyone
            if (GetComponent<ProjectileWeapon>())
            {
                GetComponent<ProjectileWeapon>().enabled = setter;
            }

            else if (GetComponent<RaycastWeapon>())
            {
                GetComponent<RaycastWeapon>().enabled = setter; 
            }

            GetComponent<Health>().enabled = setter;
        }
        else if (PhotonNetwork.CurrentRoom.CustomProperties.ContainsValue("dr"))
        {
            bool setter = photonView.IsMine;
            // Enable shooting if it is yours
            if (GetComponent<ProjectileWeapon>())
            {
                GetComponent<ProjectileWeapon>().enabled = setter;
            }

            else if (GetComponent<RaycastWeapon>())
            {
                GetComponent<RaycastWeapon>().enabled = setter;
            }

            GetComponent<Health>().enabled = setter;
        }
    }
}
