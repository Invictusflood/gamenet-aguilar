using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using System;

public class Bow : MonoBehaviourPunCallbacks
{
    [SerializeField] private GameObject owner;
    [SerializeField] private PlayerData playerData;

    public GameObject arrow;
    public float maxLaunchForce;
    public float minLaunchForce;
    public float forcePerSec;
    public float curlaunchForce;
    public float arrowGravScale;
    public Transform shotPoint;

    public GameObject point;
    GameObject[] points;
    public int numOfPoints;
    public float spaceBetweenPoints;
    Vector2 direction;

    // Start is called before the first frame update
    void Start()
    {
        //points = new GameObject[numOfPoints];
        //for (int i = 0; i < numOfPoints; i++)
        //{
        //    points[i] = Instantiate(point, shotPoint.position, Quaternion.identity);
        //}
        curlaunchForce = minLaunchForce;
    }

    private void OnEnable()
    {
        points = new GameObject[numOfPoints];
        for (int i = 0; i < numOfPoints; i++)
        {
            points[i] = Instantiate(point, shotPoint.position, Quaternion.identity);
        }
    }

    private void OnDisable()
    {
        for (int i = 0; i < numOfPoints; i++)
        {
            Destroy(points[i]);
        }
        Array.Clear(points, 0, points.Length);
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 bowPosition = transform.position;
        Vector2 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        direction = mousePosition - bowPosition; ;

        transform.right = direction;

        if (Input.GetButton("Fire1"))
        {
            if (curlaunchForce < maxLaunchForce)
            {
                curlaunchForce += forcePerSec * Time.deltaTime;
            }
        }

        if (Input.GetButtonUp("Fire1"))
        {
            Shoot();
            curlaunchForce = minLaunchForce;
        }

        for (int i = 0; i < numOfPoints; i++)
        {
            points[i].transform.position = PointPosition(i * spaceBetweenPoints);
        }
    }

    void Shoot()
    {
        if (playerData.ammo > 0)
        {
            //playerData.ammo--;
            playerData.photonView.RPC("RemoveAmmo", RpcTarget.AllBuffered);
            //GameObject newArrow = Instantiate(arrow, shotPoint.position, shotPoint.rotation);
            GameObject newArrow = PhotonNetwork.Instantiate(arrow.name, shotPoint.position, shotPoint.rotation);
            Rigidbody2D arrowRb = newArrow.GetComponent<Rigidbody2D>();
            arrowRb.gravityScale = arrowGravScale;
            arrowRb.velocity = transform.right * curlaunchForce;
        }
    }

    Vector2 PointPosition(float time)
    {
        Vector2 pos = (Vector2)shotPoint.position + (direction.normalized * curlaunchForce * time) + 0.5f * (Physics2D.gravity * arrowGravScale) * (time * time);
        return pos;
    }
}
